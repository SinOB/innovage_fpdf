=== Innovage FPDF ===
Contributors: SinOB
Tags: buddypress
Requires at least: 3.9
Tested up to: 3.9
Stable tag: 0.1-alpha
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Generate an instruction and info sheet for group manager to provide to group members.


== Description ==

When called will generate a page GroupAndChallengeInfo.pdf. Generated PDF is 
currently empty. A page must be created on wordpress to hold the called page 
with shortcode [view_admin_challenge_file]. Call to page must be embedded 
somewhere in the template. Plugin incomplete. 

There are currently no admin options.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
