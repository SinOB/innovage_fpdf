<?php

/**
  Plugin Name: Innovage FPDF
  Plugin URI: https://bitbucket.org/SinOB/innovage_fpdf
  Description: Custom simple fpdf generator - intend to generate instruction pdf
  for group members. Incomplete due to lack of requirement specification.
  (Wanted a printable but did not want to say what it needed to print)
  Author: Sinead O'Brien
  Version: 1.0-alpha
  Author URI: https://bitbucket.org/SinOB
  Requires at least: 3.9
  Tested up to: 3.9
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */
require('fpdf/fpdf.php');

class Innovage_PDF extends FPDF {

// Page header
    function Header() {
        // Logo
        //$this->Image('logo.png', 10, 6, 30);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        //$this->Cell(80);
        // Title
        $this->Cell(0, 10, 'iStep - Intergenerational Support To Encourage Physical Activity', 0, 0, 'C');
        // Line break
        $this->Ln(10);

        $this->SetTextColor(183, 13, 80);
        $this->Cell(0, 10, 'iStep Pilot Study', 0, 0, 'C');
        $this->Ln(10);
        $this->Cell(0, 10, 'Information for Students', 0, 0, 'C');
        $this->Ln(20);
    }

// Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    function h2($text) {
        $this->SetFont('Arial', 'B', 12);
        $this->SetTextColor(183, 13, 80);
        $this->Cell(0, 10, $text);
        $this->Ln(10);
    }

    function paragraph($text) {
        $this->SetFont('Times', '', 12);
        $this->SetTextColor(0, 0, 0);
        $this->MultiCell(0, 7, $text, 0, 1);
        // add space at end of paragraph
        $this->Ln(5);
    }

    function bulleted($text) {
        $bullet = chr(149);
        $this->SetFont('Times', '', 12);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(20);
        $this->Cell(0, 7, $bullet . $text, 0, 1);
    }

}

function innovage_fpdf_generate_file() {
// Instanciation of inherited class
    $pdf = new Innovage_PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetFont('Times', '', 12);

    $pdf->h2('What is this research about?');
    $pdf->paragraph("This project's name is iStep. It is about finding new, fun ways to get people to exercise by doing more walking. We will do it for the next few weeks.");
    $pdf->h2('Who are we?');
    $pdf->paragraph("We are from Sheffield Hallam University.");
    $pdf->h2('What do we want to find out?');
    $pdf->paragraph("In this project we want to find out:");
    $pdf->bulleted("Point 1");
    $pdf->bulleted("Point 2");
    $pdf->bulleted("Point 3");
    $pdf->Ln(5);

    $pdf->h2('Lorem ipsum dolor sit amet');
    $pdf->paragraph('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo tempor, venenatis metus ac, semper ligula. Suspendisse placerat vehicula mattis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam ex dui, imperdiet eu mauris ac, tempor gravida ipsum. Donec a volutpat neque, a tempus sem. Donec quis placerat risus. Maecenas varius odio ut nunc euismod dapibus. Phasellus risus enim, fringilla in tempus in, finibus eget lacus. Aliquam dictum magna at purus congue, vitae venenatis justo commodo. Aenean sollicitudin rhoncus orci, quis malesuada ipsum vehicula eget. Vivamus nec elit lorem. Vestibulum tortor odio, sollicitudin a nibh vel, mattis efficitur nisi. Donec porttitor tempor suscipit. Nunc gravida consequat aliquet. In quis pulvinar quam. Nunc vel metus a nulla luctus faucibus at eget ipsum. ');

    $pdf->h2('Lorem ipsum dolor sit amet');
    $pdf->paragraph('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo tempor, venenatis metus ac, semper ligula. Suspendisse placerat vehicula mattis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam ex dui, imperdiet eu mauris ac, tempor gravida ipsum. Donec a volutpat neque, a tempus sem. Donec quis placerat risus. Maecenas varius odio ut nunc euismod dapibus. Phasellus risus enim, fringilla in tempus in, finibus eget lacus. Aliquam dictum magna at purus congue, vitae venenatis justo commodo. Aenean sollicitudin rhoncus orci, quis malesuada ipsum vehicula eget. Vivamus nec elit lorem. Vestibulum tortor odio, sollicitudin a nibh vel, mattis efficitur nisi. Donec porttitor tempor suscipit. Nunc gravida consequat aliquet. In quis pulvinar quam. Nunc vel metus a nulla luctus faucibus at eget ipsum. ');

    $pdf->h2('Lorem ipsum dolor sit amet');
    $pdf->paragraph('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at justo tempor, venenatis metus ac, semper ligula. Suspendisse placerat vehicula mattis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam ex dui, imperdiet eu mauris ac, tempor gravida ipsum. Donec a volutpat neque, a tempus sem. Donec quis placerat risus. Maecenas varius odio ut nunc euismod dapibus. Phasellus risus enim, fringilla in tempus in, finibus eget lacus. Aliquam dictum magna at purus congue, vitae venenatis justo commodo. Aenean sollicitudin rhoncus orci, quis malesuada ipsum vehicula eget. Vivamus nec elit lorem. Vestibulum tortor odio, sollicitudin a nibh vel, mattis efficitur nisi. Donec porttitor tempor suscipit. Nunc gravida consequat aliquet. In quis pulvinar quam. Nunc vel metus a nulla luctus faucibus at eget ipsum. ');

    $pdf->Output();
}

add_action('template_redirect', 'innovage_fpdf_template_redirect');

function innovage_fpdf_template_redirect() {
    $name = 'groupchallengeinfo.pdf';
    if (strpos($_SERVER["REQUEST_URI"], $name) !== false) {
        header('Content-Type: application/pdf', true, 200);
        header('Content-Disposition: inline; filename="' . $name . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        innovage_fpdf_generate_file();

        exit();
    }
}

//
?>
